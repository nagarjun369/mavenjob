package com.flipkart;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class mousehover {
	WebDriver driver;
	@BeforeMethod
	public void beforeMethods() throws InterruptedException
	
	{
System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
		
driver = new ChromeDriver();//runtime polymorphism - method
		
		driver.manage().window().maximize();
		
		driver.get("https://www.flipkart.com");
		
		Thread.sleep(3000);
	}
	
	@AfterMethod
	public void afterMethods()
	{
		driver.close();
	}
	
	@Test
	public void mousehover() throws Throwable
		{
		
		driver.findElement(By.xpath("//div/div/div/button")).click();
		WebElement HoverAccountList=driver.findElement(By.xpath("//span[text()='Electronics']"));
		
		Actions act = new Actions(driver);
		
		act.moveToElement(HoverAccountList).build().perform();
		Thread.sleep(3000);

		
	List<WebElement> AccounThover=	driver.findElements(By.xpath("//div[@id='container']/div/div[@class='zi6sUf']/div/ul/li/ul/li/ul/li/ul/li"));
	
	System.out.println(AccounThover.size());
	
	
	for(int i = 0 ; i<AccounThover.size();i++)
	{
		System.out.println(AccounThover.get(i).getText());
	}
	}

}
