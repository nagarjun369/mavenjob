package com.flipkart;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class loginpage {

	
WebDriver driver;
	
	
	@BeforeMethod
	public void beforeMethods() throws InterruptedException
	
	{
System.setProperty("webdriver.chrome.driver", "D:\\chromedriver\\chromedriver.exe");
		
driver = new ChromeDriver();//runtime polymorphism - method
		
		driver.manage().window().maximize();
		
		driver.get("https://www.flipkart.com");
		
		Thread.sleep(3000);
	}
	
	@AfterMethod
	public void afterMethods()
	{
		driver.close();
	}
	
    @Test(priority=1)
    public void wrongusername() throws Throwable
    {
		/*
		 * driver.findElement(By.xpath("//a[text()='Login']")).click();
		 * Thread.sleep(4000);
		 */
				
				driver.findElement(By.xpath("(//div/input[@type='text'])[2]")).sendKeys("jjjdjdj");
		    	driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Workout@1");
		    	driver.findElement(By.xpath("//div[@class='_1avdGP']/button[@type='submit' ]")).click();
		    	String str=driver.findElement(By.xpath("//span[@class='ZAtlA-']/span")).getText();
				Thread.sleep(3000);
				System.out.println(str);
				//driver.close();
				
			}
    
    @Test(priority=2)
    public void nopassword() throws Throwable
    {
    	driver.findElement(By.xpath("(//div/input[@type='text'])[2]")).sendKeys("arjun.079@gmail.com");
    	
    	driver.findElement(By.xpath("//div[@class='_1avdGP']/button[@type='submit' ]")).click();
    	String str=driver.findElement(By.xpath("//span[@class='ZAtlA-']/span")).getText();
		Thread.sleep(3000);
		System.out.println(str);
		//driver.close();
    	
    }
    
    @Test(priority=3)
    public void wrongpassword() throws Throwable
    {
		/*
		 * driver.findElement(By.xpath("//a[text()='Login']")).click();
		 * Thread.sleep(4000);
		 */
				
				driver.findElement(By.xpath("(//div/input[@type='text'])[2]")).sendKeys("arjun.079@gmail.com");
		    	driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Workout@2");
		    	driver.findElement(By.xpath("//div[@class='_1avdGP']/button[@type='submit' ]")).click();
		    	Thread.sleep(4000);
		    	String str=driver.findElement(By.xpath("//span/span[text()='Your username or password is incorrect']")).getText();
				Thread.sleep(3000);
				System.out.println(str);
				//driver.close();
				
			}
		
		
		
    	
    
}
